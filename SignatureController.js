({
    Init : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    erase:function(component, event, helper){
        helper.eraseHelper(component, event, helper);
    },
    save:function(component, event, helper){
        var isBlank = helper.isCanvasBlank(component.find('can').getElement());
      //  console.log('Is Canvas Sign Blank ->'+isBlank);
        if(!isBlank){
            helper.saveHelper(component, event, helper);
        }
        else{
            helper.errorToastMessage(component,event);
        }
   }
})