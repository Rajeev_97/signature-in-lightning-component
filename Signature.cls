public without sharing class NYCParks_SignatureController {
    @AuraEnabled
    public static Id saveSignature(String signatureBody, Id applicationId, String signType) {

        List < Attachment > att = [SELECT Id, Name, Body, ContentType, ParentId From Attachment where ParentId =: applicationId 
        LIMIT 1
        ];
      
        System.debug('attachment is ' + att);
        Attachment a = new Attachment();
      
            if (!att.isEmpty()) {
                a.parentId = att[0].ParentId;
                a.Id = att[0].Id;
                a.Body = EncodingUtil.base64Decode(signatureBody);
                update a;
                system.debug('a>>>>' + a.Id);

            } else {
                a.ParentId = applicationId; // record id of object.
                a.Body = EncodingUtil.base64Decode(signatureBody);
                a.ContentType = 'image/jpeg';
                a.Name = 'EmployeeSignature Capture.jpeg';
                System.debug('a value is ' + a);
                insert a;
            }
            System.debug('a value is '+a);
            return a.id;
      
    }  

    
    @AuraEnabled
    public static list < Attachment > getImage(String applicationId) {
      return [select Id, Name, ContentType, body from Attachment
            where parentid =: applicationId and ContentType in ('image/png', 'image/jpeg', 'image/gif')
        ];
    }
}